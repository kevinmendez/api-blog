from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views

from django.conf import settings
import oauth2_provider.views as oauth2_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/1.0/', include(('apps.accounts.urls', 'oauth-routes') , namespace="oauth-routes")),
    path('api/1.0/', include(('apps.blog.urls', 'blog-routes'), namespace="blog-routes")),
]

if settings.DEBUG:
    
    urlpatterns += [
        path("o/", include('oauth2_provider.urls', namespace='oauth2_provider')),
    ]
    urlpatterns += [
        path('applications/', oauth2_views.ApplicationList.as_view(), name="list"),
        path('applications/register/', oauth2_views.ApplicationRegistration.as_view(), name="register"),
        path('applications/<pk>/', oauth2_views.ApplicationDetail.as_view(), name="detail"),
        path('applications/<pk>/delete/', oauth2_views.ApplicationDelete.as_view(), name="delete"),
        path('applications/<pk>/update/', oauth2_views.ApplicationUpdate.as_view(), name="update"),
    ]

    # OAuth2 Token Management endpoints
    urlpatterns += [
        path('authorized-tokens/', oauth2_views.AuthorizedTokensListView.as_view(), name="authorized-token-list"),
        path('authorized-tokens/<pk>/delete/', oauth2_views.AuthorizedTokenDeleteView.as_view(),
            name="authorized-token-delete"),
    ]


#curl -X POST http://localhost:8000/o/token/ -H "content-type: application/x-www-form-urlencoded" -d "grant_type=password&client_id=kOZxW9c5NYOYXj43kJebpSPxEmCCUP4kp2rR6fYl&client_secret=YoFDqEQXW2KYjxIA3Kr6DsdysrsZbYM35swxYzWsItO1ITxnMehhHKHE6sHwehMXOmIPosjINJ8vp92iig46RdhCy66yZP783HBiUqSXEsRu3GRvQqrZSs2Wvh2DVvxh&username=admin&password=secret