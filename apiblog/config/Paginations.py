from rest_framework import pagination

class CustomAPIPagination(pagination.PageNumberPagination):
    page_size = 5
