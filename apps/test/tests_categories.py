from .tests_base import BaseTest
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from apps.blog.models import Categoria

class CategorieTest(BaseTest):
    # LIST
    def test_get_all_categories(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        response = self.client.get(api_reverse("blog-routes:categoria_list"), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_categories_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        response = self.client.get(api_reverse("blog-routes:categoria_list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    # RETRIEVE
    def test_retrieve_a_newly_created_categorie(self):
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.get(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('categorie'), categorie.categorie)
        categorie.delete()
    
    def test_retrieve_a_newly_created_categorie_failed(self):
        """
        Test failded for not found
        """
        idCategorie = 0
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.get(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_a_newly_created_categorie_failed_for_inauthorize(self):
        """
        Test fails for lack of inauthorization 
        """
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        response = self.client.get(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        categorie.delete()
    

    # UPDATE
    def test_update_a_newly_created_categorie(self):
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        data = {
            "categorie": "New categorie for update from test"
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.put(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('categorie'), data.get('categorie'))
    
    def test_update_a_newly_created_categorie_failed_for_inauthorize(self):
        """
        Test fails for lack of inauthorization 
        """
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        data = {
            "categorie": "New categorie for update from test"
        }
        response = self.client.put(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
    
    def test_update_a_newly_created_categorie_failed_not_found(self):
        """
        Test fails for not found 
        """
        idCategorie = 0
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        data = {
            "categorie": ""
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }

        response = self.client.put(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        
    def test_update_a_newly_created_categorie_failed(self):
        """
        Test fails for empty categorie 
        """
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        data = {
            "categorie": ""
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }

        response = self.client.put(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
    # DELETE
    def test_delete_a_newly_created_categorie(self):
        categorie = Categoria.objects.create(
            categorie="New categorie from test for delete it"
        )
        idCategorie = categorie.id
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.delete(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    
    def test_delete_a_newly_created_categorie_failed_for_inauthorize(self):
        """
        Test fails for lack of inauthorization 
        """
        categorie = Categoria.objects.create(
            categorie="New categorie from test"
        )
        idCategorie = categorie.id
        
        response = self.client.delete(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        categorie.delete()
    
    def test_delete_a_newly_created_categorie_failed_not_found(self):
        """
        Test fails for not found 
        """
        idCategorie = 0
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }

        response = self.client.delete(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}),**auth_headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_delete_a_newly_created_categorie_failed(self):
        """
        Test fails for not found 
        """
        idCategorie = 0
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
       
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        response = self.client.delete(api_reverse("blog-routes:categoria_details", kwargs=
        {"pk":int(idCategorie)}), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
     
    # CREATE
    def test_created_new_categorie(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        data = {
            "categorie": "New categorie from test"
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        response = self.client.post(api_reverse("blog-routes:categoria_list"), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_created_new_categorie_authorization_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        data = {
            "categorie": "New categorie from test"
        }
        
        response = self.client.post(api_reverse("blog-routes:categoria_list"), data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
    
    def test_created_new_categorie_failed(self):
        """
        Test fails for empty categorie
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        data = {
            "categorie": ""
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        response = self.client.post(api_reverse("blog-routes:categoria_list"), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

