from .tests_base import BaseTest
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from apps.blog.models import Categoria, Publicacion

class PublicacionesTestCase(BaseTest):
    
    def create_post(self):
        return Publicacion.objects.create(
            title = "title a actualizar",
            text = "contenid de la publicacion",
            author = self.user,
        )

    # GET
    def test_get_all_publications(self):
        """
        Test fails for lack of inauthorization 
        """
        url = api_reverse("blog-routes:publicacion_list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
    def test_get_all_publications_failed(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.get(api_reverse("blog-routes:publicacion_list"), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # CREATE
    
    def test_user_created_new_post(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        categoria = Categoria.objects.create(categorie="primera desde post")
        data = {
            "title": "desde postman",
	        "text": "Texto de blog creado desde postman",
	        "author": self.user.id,
	        "categorie": [
                categoria.id
            ]
        }
        response = self.client.post(api_reverse("blog-routes:publicacion_list"), data=data, **auth_headers, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
    def test_user_created_new_post_inauthorizate_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        categoria = Categoria.objects.create(categorie="primera desde post")
        data = {
            "title": "desde postman",
	        "text": "Texto de blog creado desde postman",
	        "author": self.user.id,
	        "categorie": [
                categoria.id
            ]
        }
        response = self.client.post(api_reverse("blog-routes:publicacion_list"), data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        categoria.delete()

    def test_user_created_new_post_failed(self):
        """
        Test fails for empty title and text
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        data = {
            "title": "",
	        "text": "",
	        "author": self.user.id,
	        "categorie": [
            ]
        }
        response = self.client.post(api_reverse("blog-routes:publicacion_list"), data=data, **auth_headers, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
    # UPDATE
    def test_user_update_new_post(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        categoria = Categoria.objects.create(categorie="primera desde post")
        
        post = self.create_post()
        data = {
            "title": "Title a actualizar",
	        "text": "Texto actualizado",
	        "author": self.user.id,
	        "categorie": [
            ]
        }
        response = self.client.put(api_reverse("blog-routes:publicacion_details", kwargs={"pk":post.id}), 
            data=data, **auth_headers, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(response.data.get('updated'))
        self.assertEqual(str(response.data.get('post').get('title')), str(data.get('title')))

    def test_user_update_new_post_inauthorizate_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        post = self.create_post()
        
        data = {
            "title": "desde postman",
	        "text": "Texto de blog creado desde postman",
	        "author": self.user.id,
	        "categorie": [
            ]
        }
        
        response = self.client.put(api_reverse("blog-routes:publicacion_details", kwargs={"pk":post.id}), 
            data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        post.delete()

    def test_user_update_new_post_failed_not_found(self):
        """
        Test fails for not found
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        data = {
            "title": "title of publication",
	        "text": "New cool content",
	        "author": self.user.id,
	        "categorie": [
            ]
        }
        response = self.client.put(api_reverse("blog-routes:publicacion_details", kwargs={"pk":0}), 
            data=data, **auth_headers,  format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_user_update_new_post_failed(self):
        """
        Test fails for empty title and text
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        data = {
            "title": "",
	        "text": "",
	        "author": self.user.id,
	        "categorie": [
            ]
        }
        post = self.create_post()
        
        response = self.client.put(api_reverse("blog-routes:publicacion_details", kwargs={"pk":1}), 
            data=data, **auth_headers,  format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    # DELETE
    
    def test_user_delete_new_post(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        post = self.create_post()
        
        
        response = self.client.delete(api_reverse("blog-routes:publicacion_details", 
            kwargs={"pk":post.id}), 
            **auth_headers, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        
    def test_user_delete_new_post_inauthorizate_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        post = self.create_post()

        response = self.client.delete(api_reverse("blog-routes:publicacion_details", kwargs={"pk":post.id}), 
            format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        post.delete()

    def test_user_delete_new_post_failed_not_found(self):
        """
        Test fails for not found
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.delete(api_reverse("blog-routes:publicacion_details", kwargs={"pk":0}), 
            **auth_headers,  format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    # RETRIEVE
    def test_user_retrieve_new_post(self):
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        post = self.create_post()
        
        response = self.client.get(api_reverse("blog-routes:publicacion_details", 
            kwargs={"pk":post.id}), 
            **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        post.delete()
        
    def test_user_retrive_new_post_inauthorizate_failed(self):
        """
        Test fails for lack of inauthorization 
        """
        post = self.create_post()

        response = self.client.get(api_reverse("blog-routes:publicacion_details", kwargs={"pk":post.id}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        post.delete()

    def test_user_retrieve_new_post_failed_not_found(self):
        """
        Test fails for not found
        """
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')

        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.get(api_reverse("blog-routes:publicacion_details", kwargs={"pk":0}), 
            **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    