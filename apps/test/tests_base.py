from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from oauth2_provider.models import get_application_model

User = get_user_model()
Application = get_application_model()

class BaseTest(APITestCase):
    
    def setUp(self):
        user = User.objects.create(username="kevinmendez", email="kevinmendez@mail.com")
        user.set_password("secret")
        user.is_active = True
        user.save()
        self.user = user

        self.application = Application.objects.create(
            name="test_client_credentials_app",
            user=self.user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_PASSWORD,
        )

    def user_has_login(self):
        token_request_data = {
            "grant_type": "password",
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "username": self.user.username,
            "password": "secret",
        }

        return self.client.post(api_reverse("oauth-routes:api_0auth_login"), data=token_request_data)
    