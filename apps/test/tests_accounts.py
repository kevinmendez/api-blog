from .tests_base import BaseTest
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse

class AccountsTest(BaseTest):
    def test_user_login_success_2(self):
        response = self.user_has_login()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_login_failed(self):
        """
        Test fails for bad username
        """
        token_request_data = {
            "grant_type": "password",
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "username": "userfail",
            "password": "secret",
        }

        response = self.client.post(api_reverse("oauth-routes:api_0auth_login"), data=token_request_data)
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
    def test_user_register_success(self):
        token_request_data = {
            "grant_type": "password",
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "username": "newusername",
            "password": "secret",
            "password_confirmed": "secret",
            "first_name": "Kevin",
            "last_name": "Mendez",
            "email": "kevinmendez30@mail.com",
        }
        response = self.client.post(api_reverse("oauth-routes:api_0auth_register"), data=token_request_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
    def test_user_register_failed(self):
        """
        Test fails for lack of email 
        """
        token_request_data = {
            "grant_type": "password",
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "username": "newusername",
            "password": "secret",
            "password_confirmed": "secret",
            "first_name": "Kevin",
            "last_name": "Mendez",
        }
        response = self.client.post(api_reverse("oauth-routes:api_0auth_register"), data=token_request_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_logout_success(self):
        data =  self.user_has_login()
        token = data.data.get('access_token')
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        token_request_data = {
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "token": token
        }
        response = self.client.post(api_reverse("oauth-routes:api_0auth_logout"), data=token_request_data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
    def test_user_logout_token_authentication_failure(self):
        """
        Test fails for lack of token authentication
        """
        data =  self.user_has_login()
        token = data.data.get('access_token')
        token_request_data = {
            "grant_type": "password",
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
            "token": token
        }
        response = self.client.post(api_reverse("oauth-routes:api_0auth_logout"), data=token_request_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
    def test_user_logout_token_failure(self):
        """
        Test fails for lack of token in data
        """
        data =  self.user_has_login()
        token = data.data.get('access_token')
        token_request_data = {
            "client_id": self.application.client_id,
            "client_secret": self.application.client_secret,
        }
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        response = self.client.post(api_reverse("oauth-routes:api_0auth_logout"), data=token_request_data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
