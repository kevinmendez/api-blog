from .tests_base import BaseTest
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from apps.blog.models import *
from django.contrib.auth import get_user_model

User = get_user_model()

class CommentsTestCase(BaseTest):
    # GET
    def test_get_all_comments_by_post(self):
    
        publication = Publicacion.objects.create(
            title ="Primer post",
            text = "Texto del post",
            author = self.user,
        )

        for n in range(1, 10):
            Comentario.objects.create(
                text =  "Este es un comentario "+ str(n),
                author =  self.user,
                publicacion =  publication
            )


        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        
        response = self.client.get(api_reverse("blog-routes:comentario_detail", 
        kwargs={"pk": publication.id}), **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(response.data), 10)

    # CREATE
    def test_create_new_comment(self):
        
        user_login = self.user_has_login()
        token = user_login.data.get('access_token')
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + token,
        }
        publication = Publicacion.objects.create(
            title ="Segundo post",
            text = "Texto del post",
            author = self.user,
        )
        data = {
            "text": "Este es un comentario",
            "author": self.user.id,
            "publicacion": publication.id
        }
        response = self.client.post(api_reverse("blog-routes:comentario_detail",
        kwargs={"pk": publication.id}), data=data, **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)