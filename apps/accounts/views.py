import json
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.contrib.auth.models import User
from oauth2_provider.views import TokenView, RevokeTokenView
from apps.accounts.serializers import UserSerializers
from django.contrib.auth import get_user_model

from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from oauth2_provider.contrib.rest_framework import (TokenHasReadWriteScope, 
    TokenHasScope, OAuth2Authentication)
from rest_framework.permissions import IsAuthenticated

User = get_user_model()
class LoginAPI(APIView):
    authentication_classes = []
    permission_classes     = []

    def post(self, request, *args, **kwargs):
        url, headers, body, status_code = TokenView().create_token_response(request)
        tokenInfo = json.loads(body)


        if tokenInfo.get('error') is None:
            user = User.objects.get(username=request.data.get("username"))
            tokenInfo['user'] =  {
                "username": user.username,
                "email": user.email,
            }
        else:
            errorMessage = {
                "detail": "Invalid credentials given."
            }
            tokenInfo = errorMessage
            
        return Response(tokenInfo, status=status_code)

class LogoutAPI(APIView):

    permission_classes = [IsAuthenticated, TokenHasReadWriteScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    def post(self, request, *args, **kwargs):
        url, headers, body, status_code = RevokeTokenView().create_revocation_response(request)
        return Response({
            "logout": "success"
        }, status=status_code)

class RegisterAPI(APIView):
    authentication_classes = []
    permission_classes     = []

    def post(self, request, *args, **kwargs):
        serializer = UserSerializers(data = request.data)
        if serializer.is_valid():
            user = serializer.save()
            
            url, headers, body, status_code = TokenView().create_token_response(request)
            tokenInfo = json.loads(body)

            tokenInfo['user'] = {
                "username": user.username,
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name,

            }
            return Response(tokenInfo, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

