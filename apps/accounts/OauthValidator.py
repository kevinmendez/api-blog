from oauth2_provider.oauth2_validators import OAuth2Validator
from django.contrib.auth import authenticate

class MyOAuth2Validator(OAuth2Validator):
    def validate_user(self, username, password, client, request, *args, **kwargs):
        """
        Check username and password correspond to a valid and active User
        """
        u = authenticate(username=username, password=password)

        if u is not None and u.is_active:
           request.user = u
           return True

        return False