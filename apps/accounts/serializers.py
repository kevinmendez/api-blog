from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializers(serializers.Serializer):
    # Parametros que se reciben para crear un nuevo usuario
    id = serializers.ReadOnlyField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()
    password_confirmed = serializers.CharField()

    def create(self, validate_data):
        # validate_data is a dictionary
        # We make a instance of User
        instance = User()
        instance.first_name = validate_data.get('first_name')
        instance.last_name = validate_data.get('last_name')
        instance.username = validate_data.get('username')
        instance.email = validate_data.get('email')
        instance.set_password(validate_data.get('password'))
        instance.save()
        return instance
    
    def validate(self, data):
        pw = data.get("password")
        pw2 = data.get("password_confirmed")
        
        if pw2 is None:
            raise serializers.ValidationError({
            "password_confirmed": "password_confirmed is required"
        })

        if pw != pw2:
            raise serializers.ValidationError({
            "error": "Las contraseñas no coinciden"
        })
        return data

    def validate_username(self, data):
        users = User.objects.filter(username = data)
        if len(users) != 0:
            raise serializers.ValidationError("Este usuario ya existe, ingrese uno nuevo")
        else:
            return data

    def validate_email(self, data):
        users = User.objects.filter(email = data)
        if len(users) != 0:
            raise serializers.ValidationError("Este email ya existe, ingrese uno nuevo")
        else:
            return data
