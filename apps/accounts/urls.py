from django.urls import path
from .views import LoginAPI, RegisterAPI, LogoutAPI

urlpatterns = [
    path('0auth_login/', LoginAPI.as_view(), name="api_0auth_login"),
    path('0auth_register/', RegisterAPI.as_view(), name="api_0auth_register"),
    path('0auth_logout/', LogoutAPI.as_view(), name="api_0auth_logout"),
]