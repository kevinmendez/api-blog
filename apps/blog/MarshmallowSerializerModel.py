# Warning
# Validation occurs on deserialization but not on serialization. 
# To improve serialization performance, data passed to 
# Schema.dump() are considered valid.
from marshmallow import pprint
from marshmallow import Schema, fields, ValidationError, validate

class CategorieSchema(Schema):
    categorie = fields.Str(validate=validate.Length(min=3), required = True)


class ComentarioSchema(Schema):
    text = fields.Str(validate=validate.Length(min=3), required = True)
    author = fields.Int(required= True)
    publicacion = fields.Int(required= True)

class PublicacionSchema(Schema):
    title = fields.Str(validate=validate.Length(min=3), required = True)
    text = fields.Str(validate=validate.Length(min=3), required = True)
    author = fields.Int(required= True)
    categorie = fields.List(fields.Int(), required = True)


# Function that validate all params required
def validateSchema(schema, categorie_data):
    """
        if all is ok: return True, else, return a list with all errors
    """
    try:
        schema.load(categorie_data)
        return True

    except ValidationError as err:
        return err.messages

def validateWithoutDeserializer(schema, categorie_data):

    """
        In case that you just need to do validated the data.
        if all is ok: return an object empty, else, return a list with all errors
    """
    try:
        return schema.validate(categorie_data)

    except ValidationError as err:
        return err.messages


    