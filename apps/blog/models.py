from django.db import models
from django.utils import timezone

class Categoria(models.Model):
    categorie = models.CharField(max_length=200)
    def __str__(self):
        return self.categorie


class Publicacion(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(default=timezone.now, blank=True, null=True)

    # relationships
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    categorie = models.ManyToManyField(Categoria, blank=True)
        
    def __str__(self):
        return self.title


class Comentario(models.Model):
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    # relationships
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    publicacion = models.ForeignKey(Publicacion, related_name="comentarios", on_delete=models.CASCADE)
        
    def __str__(self):
        return self.text