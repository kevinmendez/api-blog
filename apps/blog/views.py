from rest_framework.views import APIView
from rest_framework import generics, status, mixins
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.http import Http404
from .models import *
from .Serializers import *
from .MarshmallowSerializerModel import (CategorieSchema, validateSchema, 
    PublicacionSchema, ComentarioSchema)

from oauth2_provider.contrib.rest_framework import (TokenHasScope, 
    OAuth2Authentication, IsAuthenticatedOrTokenHasScope)

class CategorieListPostGet(
    mixins.CreateModelMixin, 
    generics.ListAPIView):
    
    permission_classes = [IsAuthenticated, IsAuthenticatedOrTokenHasScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    serializer_class = CategoriaSerializer
    required_scopes = ["read", "create"]
    search_fields            = ('categorie',)
    ordering_fields          = ('categorie', "id",)
    queryset                 = Categoria.objects.all().order_by('-id')


    def post(self, request):
        data = self.serializer_class(data=request.data)
        
        # Validation with Marshmallow
        schema = CategorieSchema()
        validate = validateSchema(schema, request.data)

        if validate == True:
            if data.is_valid():
                data.save()
                return Response(data.data, status=status.HTTP_201_CREATED)

            error = data.errors   
        else: 
            error = validate
        
        return Response(error, status=status.HTTP_400_BAD_REQUEST)
    

class CategoriaListPutDelete(mixins.UpdateModelMixin, 
    mixins.DestroyModelMixin, generics.RetrieveAPIView):
    
    permission_classes = [IsAuthenticated, IsAuthenticatedOrTokenHasScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    serializer_class = CategoriaSerializer
    required_scopes = ['update', 'write', 'delete']

    queryset = Categoria.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class PublicacionListPostGet(
    mixins.CreateModelMixin, 
    generics.ListAPIView):
    
    permission_classes = [IsAuthenticated, IsAuthenticatedOrTokenHasScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    serializer_class = PublicacionSerializer
    required_scopes = ['create', 'write']
    queryset        = Publicacion.objects.all().order_by('-id')
    search_fields   = ("title", "text",)
    ordering_fields   = ("title", "text",)

    def post(self, request):
        data = self.serializer_class(data=request.data)
        # Validation with Marshmallow
        schema = PublicacionSchema()
        validate = validateSchema(schema, request.data)

        if validate == True:
            if data.is_valid():
                data.save()
                return Response(data.data, status=status.HTTP_201_CREATED)

            error = data.errors   
        else: 
            error = validate
        
        return Response(error, status=status.HTTP_400_BAD_REQUEST)
    

class PublicacionListPutDeleteRetrieve(APIView):
    
    permission_classes = [IsAuthenticated, IsAuthenticatedOrTokenHasScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    serializer_class = PublicacionSerializer
    required_scopes = ['update', 'write', 'delete']

    def get_object(self, pk):
        try: 
            return Publicacion.objects.get(pk=pk)
        except Publicacion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        publication = self.get_object(pk)
        data = self.serializer_class(publication).data
        return Response(data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        publicacion = self.get_object(pk)
        data = self.serializer_class(publicacion, data=request.data)
        
        # Validation with Marshmallow
        schema = PublicacionSchema()
        validate = validateSchema(schema, request.data)

        if validate == True:
            if data.is_valid():
                data.save()
                return Response({
                    "updated": True,
                    "post": data.data
                }, status=status.HTTP_201_CREATED)
                
            error = data.errors   
        else: 
            error = validate
        return Response(error, status=status.HTTP_400_BAD_REQUEST)
    
    
    def delete(self, request, pk, format=None):
        post = self.get_object(pk)
        data = self.serializer_class(post).data
        post.delete()

        return Response({"deleted": True, "post": data,}, status=status.HTTP_204_NO_CONTENT)


class ComentarioListGet(
    generics.CreateAPIView, 
    generics.ListAPIView):
    
    permission_classes = [IsAuthenticated, IsAuthenticatedOrTokenHasScope]
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    required_scopes = ['create', 'write']
    serializer_class = ComentarioSerializer

    def get_queryset(self):
        _id = self.kwargs.get('pk')
        comentario = Comentario.objects.filter(publicacion=_id).order_by('-id')
        if len(comentario) != 0:
            return comentario
        
        raise Http404

    def post(self, request, pk):
        data = self.serializer_class(data=request.data)
        
        # Validation with Marshmallow
        schema = ComentarioSchema()
        validate = validateSchema(schema, request.data)

        if validate == True:
            if data.is_valid():
                data.save()
                return Response(data.data, status=status.HTTP_201_CREATED)

            error = data.errors   
        else: 
            error = validate
        
        return Response(error, status=status.HTTP_400_BAD_REQUEST)


 
