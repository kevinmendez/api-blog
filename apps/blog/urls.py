from django.urls import path
from .views import *

urlpatterns = [
    path('categoria/', CategorieListPostGet.as_view(), name="categoria_list"),
    path('categoria/<pk>', CategoriaListPutDelete.as_view(), name="categoria_details"),
    path('publicacion/', PublicacionListPostGet.as_view(), name="publicacion_list"),
    path('publicacion/<pk>', PublicacionListPutDeleteRetrieve.as_view(), name="publicacion_details"),
    path('comentario/publicacion/<pk>', ComentarioListGet.as_view(), name="comentario_detail"),
]