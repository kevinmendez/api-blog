from rest_framework import serializers
from .models import Categoria, Publicacion, Comentario
from apps.accounts.serializers import UserSerializers
from django.contrib.auth.models import User

class CategoriaSerializer(serializers.ModelSerializer):
    id = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Categoria
        fields = ("id", "categorie",)

class UserRelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "username", "email",)

        
class ComentarioSerializer(serializers.ModelSerializer):
    # author = UserRelationSerializer()
    class Meta:
        model = Comentario
        fields = ("id", "text","author","publicacion")

class PublicacionSerializer(serializers.ModelSerializer):
    # categorie = CategoriaSerializer(many=True)
    # author = UserRelationSerializer()
    # comentarios = ComentarioSerializer(many=True, read_only=True)

    class Meta:
        model = Publicacion
        fields = ("id", "title", "text", "author", "categorie",)
